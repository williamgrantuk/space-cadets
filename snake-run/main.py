import random

from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.image import Image
from kivy.properties import NumericProperty, ObjectProperty, ReferenceListProperty
from kivy.vector import Vector
from kivy.clock import Clock
from kivy.config import Config
from kivy.core.window import Window

Config.set('graphics', 'width', '800')
Config.set('graphics', 'height', '600')
Config.set('graphics', 'resizable', False)
Config.write()

GRAVITY = 0.7

class Snake(Widget):
    y_velocity = NumericProperty(0)
    #ground_y = NumericProperty(0)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.is_jumping = False

        self.image = Image(source="snake_run_animation.zip", anim_delay=0.1)
        def disable_blur(x, y):
            self.image.texture.mag_filter = 'nearest'
        self.image.bind(texture=disable_blur)
        #self.image.bind(texture=lambda x, y: self.image.texture.mag_filter = 'nearest')
        #self.image.texture.mag_filter = 'nearest'

        self.ground_y = Window.height / 4

#    def on_parent(self, instance, parent):
#        self.ground_y = self.parent.height + self.height / 2 

    def jump(self):
        if not self.is_jumping:
            self.y_velocity = 20
            self.is_jumping = True

    def update(self, dt):
        if self.is_jumping:
            self.pos = Vector(0, self.y_velocity) + self.pos

            #check if the snake is on the ground
            if self.y <= self.ground_y:
                self.y = self.ground_y
                self.y_velocity = 0
                self.is_jumping = False
            else:
                self.y_velocity -= GRAVITY

    def check_hit(self, spike, on_hit):
        if self.collide_widget(spike):
            on_hit()

class Spike(Widget):
    x_velocity = NumericProperty(-5)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.is_moving = False
        self.time_until_move = random.randint(0, 120)
        self.left_edge = -self.width
        self.start_pos = Window.width
        

#    def on_parent(self, instance, parent):
#        self.left_edge = self.parent.x - self.width
#        self.start_pos = self.parent.right * 8

    def update(self, dt):
        if self.is_moving:
            self.pos = Vector(self.x_velocity, 0) + self.pos

            #reset when you hit the left edge
            if self.x <= self.left_edge:
                self.x = self.start_pos
                self.is_moving = False
        else:
            self.time_until_move -= 1
            if self.time_until_move == 0:
                self.is_moving = True
                self.x_velocity -= 0.5
                self.time_until_move = random.randint(0, 120)

class Game(Widget):
    score = NumericProperty(0)

    snake = ObjectProperty(None)

    spike1 = ObjectProperty(None)
    spike2 = ObjectProperty(None)
    spikes = ReferenceListProperty(spike1, spike2)

    image = ObjectProperty(Image(source="ground.png"))

    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)
        self.image.texture.mag_filter = 'nearest'

    def on_touch_down(self, touch):
        self.snake.jump()

    def update(self, dt):
        #print(dt)  #for testing

        self.score += 1
        
        self.snake.update(dt)
    
        for spike in self.spikes:
            self.snake.check_hit(spike, self.on_hit)
            spike.update(dt)

    def on_hit(self):
        print("hit!")

        #reset the score
        self.score = 0

        #reset the spikes
        for spike in self.spikes:
            spike.x = spike.start_pos
            spike.x_velocity = -5

class SnakerunApp(App):
    def build(self):
        game = Game()
        Clock.schedule_interval(game.update, 1/60)
        return game

if __name__ == '__main__':
    SnakerunApp().run()

