import java.net.URL;
import java.net.MalformedURLException;
import java.util.Scanner;
import java.io.IOException;

class idToName {

    public static void main(String[] args) {

        //get the email id and the web address
        String id = args[0];
        String webAddress = "https://www.ecs.soton.ac.uk/people/" + id;

        //create the URL and access the web page
        URL pageUrl = null;
        try {
            pageUrl = new URL(webAddress);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            System.exit(1);
        }

        Scanner pageScanner = null;
        try {
            pageScanner = new Scanner(pageUrl.openStream());
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(2);
        }

        StringBuffer pageBuffer = new StringBuffer();

        //read the whole page into the buffer
        while (pageScanner.hasNext()) {
            pageBuffer.append(pageScanner.next());
        }

        String webPage = pageBuffer.toString();

        //find 'property="name">' then read until '<'
        int nameLocationStart = webPage.indexOf("property=`name`>".replace('`', '"')) + 16;

        if (nameLocationStart == 15) {  //webpage.indexOf returns -1 if no match was found
            System.out.println("name not found");
            System.exit(0);
        }

        String name = "";
        int currentLocation = nameLocationStart;
        while (true) {
            char currentChar = webPage.charAt(currentLocation);
            if (currentChar == '<') {
                break;
            } else {
                name += currentChar;
            }
            currentLocation++;
        }

        //add spaces in between parts of names
        String nameWithSpaces = "";
        for (char c : name.toCharArray()) {
            if (Character.isUpperCase(c)) {
                nameWithSpaces += ' ';
            }
            nameWithSpaces += c;
        }

        //strip the extraneous space from the beginning
        if (nameWithSpaces.charAt(0) == ' ') {
            nameWithSpaces = nameWithSpaces.substring(1);
        }

        System.out.println(nameWithSpaces);

    }
}
