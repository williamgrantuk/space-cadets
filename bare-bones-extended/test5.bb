def mulThree X;
    //perform the multiplication
    incr Y;
    incr Y;
    incr Y;
    while X not 0 do;
        clear W;
        while Y not 0 do;
            incr Z;
            incr W;
            decr Y;
        end;
        while W not 0 do;
            incr Y;
            decr W;
        end;
        decr X;
    end;

    //put the answer into X
    while Z not 0 do;
        incr X;
        decr Z;
    end;
end;

incr X;
incr X;
mulThree X;
incr X;
