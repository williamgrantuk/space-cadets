# bare bones extended

an interpreter for the minimalist programing language 'bare bones' <br>
build using "gradle wrapper" then "./gradlew build" <br>
I couldn't figure out how to make a stand-alone jar file using gradle so if you want to run it you must type "./gradlew run --args="--path name-of-source-file" <br>
sorry if some of the code looks ugly, I had to learn a very complex library in just one week <br>
come to think of it, I think my solution is somewhat over-engineered. <br>

## new features

The extended version of barebones includes character input, output and the ability to define and use 1-arity functions. <br>
it also adds a compiler (WIP, currently only prints c++ source code. Does not yet support functions) <br>
to run in compile mode, type "./gradlew run --args="--path name-of-source-file --compile" <br>
