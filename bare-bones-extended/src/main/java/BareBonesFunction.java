package william.grant.BareBones;

import org.antlr.v4.runtime.*;

public class BareBonesFunction {
    private String argName;
    private BareBonesParser.ExpressionsContext body;

    public BareBonesFunction(String argName, BareBonesParser.ExpressionsContext body) {
        this.argName = argName;
        this.body = body;
    }

    public int call(Register arg) {
        //evaluate the function body
        EvalVisitor visitor = new EvalVisitor(this.argName, arg);
        visitor.visit(this.body);

        //return the value of the argument
        return visitor.getRegister(this.argName).getValue();
    }
}
