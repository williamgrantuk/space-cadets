package william.grant.BareBones;

import com.google.devtools.common.options.Option;
import com.google.devtools.common.options.OptionsBase;

public class BareBonesOptions extends OptionsBase {
    @Option(
        name = "compile",
        abbrev = 'c',
        help = "enable compilation mode.",
        defaultValue="false"
    )
    public boolean compile;

    @Option(
        name = "path",
        abbrev = 'p',
        help = "path to the barebones source file",
        defaultValue = ""
    )
    public String path;
}
