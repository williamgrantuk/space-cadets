package william.grant.BareBones;

import java.io.IOException;
import jline.console.ConsoleReader;

public class Register {
  private int value = 0;

  public void clear() {
    this.value = 0;
  }

  public void incr() {
    this.value++;
  }

  public void decr() {
    if (this.value > 0) { // registers cannot hold negative values
      this.value--;
    }
  }

  public void in(ConsoleReader reader) {
    try {
      int input = reader.readCharacter();
      if (input != -1) {  //EOF should leave the register unchanged
        this.value = input;
      }
    } catch (IOException e) {
      e.printStackTrace();
      System.exit(1);
    }
  }

  public void out() {
    System.out.print((char) this.value);
  }

  public void callFunc(BareBonesFunction func) {
    this.value = func.call(this);
  }

  public int getValue() {
    return this.value;
  }
}

