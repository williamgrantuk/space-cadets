package william.grant.BareBones;

import java.io.IOException;
import java.nio.file.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import com.google.devtools.common.options.OptionsParser;

public class Main {
  public static void main(String[] args) {

    //parse command line arguments
    OptionsParser optionsParser = OptionsParser.newOptionsParser(BareBonesOptions.class);
    optionsParser.parseAndExitUponError(args);
    BareBonesOptions options = optionsParser.getOptions(BareBonesOptions.class);

    // get the source file from the path specified in the command line
    CharStream inputStream = null;
    try {
      inputStream = CharStreams.fromFileName(options.path);
    } catch (IOException e) {
      System.err.println("could not find file " + options.path);
      System.exit(1);
    }

    // instantiate the generated lexer and parser
    BareBonesLexer lexer = new BareBonesLexer(inputStream);
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    BareBonesParser parser = new BareBonesParser(tokens);

    // instantiate and run the visitor
    BareBonesParser.BarebonesContext context = parser.barebones();
    BareBonesBaseVisitor<String> visitor;
    if (options.compile) {
      visitor = new CompilerVisitor();
      compileCpp(visitor.visit(context), options.path);
    } else {
      visitor = new EvalVisitor();
      System.out.print(visitor.visit(context));
    }
  }

  private static void compileCpp(String path, String src) {
    String[] splitPath = path.split("/");
    System.out.println(splitPath.toString());
    String name = splitPath[splitPath.length - 1];
    name = name.split(".")[0];
    System.out.println(name);

    try {
      Files.write(Paths.get(name + ".cpp"), src.getBytes());
    } catch (IOException e) {
      e.printStackTrace();
      System.exit(1);
    }

    //Process process = new ProcessBuilder("g++", "-o", name, "name" + ".cpp").start();

  }
}

