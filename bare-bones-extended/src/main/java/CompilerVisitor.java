package william.grant.BareBones;

import java.util.ArrayList;

public class CompilerVisitor extends BareBonesBaseVisitor<String> {
    private String preamble = "#include <iostream>\n#include <conio.h>\nint main() {\n";

    private ArrayList<String> registers = new ArrayList<String>();

    @Override
    public String visitBarebones(BareBonesParser.BarebonesContext context) {
        //String cppSource = this.preamble + this.visit(context.expressions());
        String cppSource = this.visit(context.expressions());
        cppSource = this.preamble + cppSource;

        //write code to ourtput the values of the registers
        String output = "std::cout";
        for (String registerName: this.registers) {
            output += String.format(" << `%1$s: ` << %1$s << std::endl".replace('`', '"'), registerName);
        }

        return cppSource + output + ";\nreturn 0;\n}";
    }

/*
    @Override
    public String visitExpressions(BareBonesParser.ExpressionsContext context) {
        return this.visitChildren(context)
    }
*/
    @Override
    public String visitCommand(BareBonesParser.CommandContext context) {
        String registerName = context.WORD(1).getText();
        this.init(registerName);

        String operator = context.WORD(0).getText();
        switch (operator) {
            case "clear":
                return registerName + " = 0;\n";
            case "incr":
                return registerName + "++;\n";
            case "decr":
                return registerName + "--;\n";
            case "in":
                return registerName + " = getch();\n";
            case "out":
                return "std::cout << (char)" + registerName + ";\n";
            default:
                return "fail\n";
        }
    }

    @Override
    public String visitWhileblock(BareBonesParser.WhileblockContext context) {
        String registerName = context.WORD().getText();
        this.init(registerName);
        String block = this.visit(context.expressions());
        return "while (" + registerName + " != 0) {\n" + block + "}\n";
    }

    @Override
    protected String aggregateResult(String aggregate, String nextResult) {
        if (nextResult == null) {
            return aggregate;
        } else if (aggregate == null) {
            return nextResult;
        } else {
            return aggregate + nextResult;
        }
    }

    private void init(String registerName) {
        if (!this.registers.contains(registerName)) {
            this.registers.add(registerName);
            this.preamble += "int " + registerName + "{0};\n";
        }
    }
}
