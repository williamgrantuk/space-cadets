package william.grant.BareBones;

import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import org.antlr.v4.runtime.*;
import jline.console.ConsoleReader;

public class EvalVisitor extends BareBonesBaseVisitor<String> {

  // specify whitch of the two WORDs in the command that we want
  public static final int OPERATOR = 0;
  public static final int OPERAND = 1;

  private Map<String, Register> registerMap = new HashMap<String, Register>();
  private Map<String, BareBonesFunction> functionMap = new HashMap<String, BareBonesFunction>();

  private ConsoleReader reader = null;

  private void setUpConsole() {
    try {
      this.reader = new ConsoleReader(System.in, System.out);
    } catch (IOException e) {
      e.printStackTrace();
      System.exit(1);
    }
  }

  //the constructor for evaluating the main code block
  public EvalVisitor() {
    this.setUpConsole();
  }

  //the constructor for evaluating functions
  public EvalVisitor(String registerName, Register register) {
    this.registerMap.put(registerName, register);
    this.setUpConsole();
  }

  @Override
  public String visitBarebones(BareBonesParser.BarebonesContext context) {
    this.visit(context.expressions());

    // return a string containing all the values of the registers
    String output = "";
    for (var entry : registerMap.entrySet()) {
      String registerName = entry.getKey();
      Register register = entry.getValue();
      output += registerName + String.format(": %d", register.getValue()) + "\n";
    }
    return output;
  }

  @Override
  public String visitCommand(BareBonesParser.CommandContext context) {

    String registerName = context.WORD(OPERAND).getText();
    Register register = this.getRegister(registerName);

    String operator = context.WORD(OPERATOR).getText();
    switch (operator) {
      case "clear":
        register.clear();
        break;
      case "incr":
        register.incr();
        break;
      case "decr":
        register.decr();
        break;
      case "in":
        register.in(this.reader);
        break;
      case "out":
        register.out();
        break;
      default:
        BareBonesFunction func = this.functionMap.get(operator);
        register.callFunc(func);
    }

    return visitChildren(context);
  }

  @Override
  public String visitWhileblock(BareBonesParser.WhileblockContext context) {
    Register register = this.getRegister(context.WORD().getText());

    while (register.getValue() != 0) {
      this.visit(context.expressions());
    }

    return null;
  }

  @Override
  public String visitFuncdef(BareBonesParser.FuncdefContext context) {
    BareBonesFunction func = new BareBonesFunction(context.WORD(1).getText(), context.expressions());
    this.functionMap.put(context.WORD(0).getText(), func);

    return null;
  }

  public Register getRegister(String registerName) {
    Register register = this.registerMap.get(registerName);

    if (register == null) { // the register does not exist
      register = new Register();
      this.registerMap.put(registerName, register);
    }

    return register;
  }
}

