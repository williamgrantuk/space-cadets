grammar BareBones;

/*
 * Parser Rules
 */

barebones   : WHITESPACE* expressions EOF ;

expressions : (funcdef | command | whileblock)+ ;

funcdef     : 'def ' WORD WHITESPACE* WORD endline expressions 'end' endline ;

command     : WORD WHITESPACE* WORD endline ;

whileblock  : 'while ' WORD ' not 0 do' endline expressions 'end' endline ;

endline     : WHITESPACE* ';' WHITESPACE* ;

/*
 * Lexer Rules
 */

COMMENT     : '//' ~[\r\n]* -> skip ;

WORD        : [a-zA-Z]+ ;

WHITESPACE  : [ \t\n\r]+ ;
