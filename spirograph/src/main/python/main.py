'''retrives the spirograph instance from java and initialises Qt'''

from py4j.java_gateway import JavaGateway
from PyQt5.QtWidgets import QApplication

from widgets import MainWindow, PY_MODE

import sys

if PY_MODE:
    from spirograph import Spirograph

if __name__ == '__main__':
    if PY_MODE:
        spirograph = Spirograph()
    else:
        #get the spirograph class
        gateway = JavaGateway()
        spirograph = gateway.entry_point.getSpirograph()

    #create the QT application
    app = QApplication(sys.argv)

    #on windows, give the process a unique AppUserModelID. This differentiates the program from the python interpreter
    try:
        from PyQt5.QtWinExtras import QtWin
    except ImportError:  #the program is running on mac/linux
        pass
    else:
        myappid = 'william.grant.spirograph'
        QtWin.setCurrentProcessExplicitAppUserModelID(myappid)

    main_window = MainWindow(spirograph)
    
    #run the QT application
    main_window.show()
    app.exec_()

