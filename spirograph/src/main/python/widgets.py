'''defines custom widgets that make up the user interface'''

import matplotlib
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

import numpy as np

matplotlib.use('Qt5Agg', force=True)  #use the Qt5 backend

PY_MODE = False

def arraylist_to_list(arraylist):
    return [arraylist.get(x) for x in range(arraylist.size())]

def arraylist_to_matrix(arraylist):
    return np.array(arraylist_to_list(arraylist))

def get_points(spirograph):
    '''calculate and return the points from the given spirograph'''

    if PY_MODE:
        return spirograph.calculate_points()

    pair = spirograph.calculatePoints()
    return arraylist_to_matrix(pair.getFirst()), arraylist_to_matrix(pair.getSecond())

def get_callback(spirograph_widget, spirograph, setter):
    '''return a callback that updates the given calls and updates the spirograph_widget'''

    def callback(new_value):
        setter(new_value)
        x, y = get_points(spirograph)
        spirograph_widget.update(x, y)

    return callback

#inheriting from a concrete class is usually a bad idea, 
#but the spec says I should "focus on creating an Object Oriented solution" 
class MainWindow(QMainWindow):
    '''the main window of the application'''

    def __init__(self, spirograph):
        #initialise the window
        super().__init__()
        self.setWindowTitle("Spirograph")

        #store the spirograph so it can be accessed by the contained widgets
        self.spirograph = spirograph

        #initialise the UI elements
        self.main_widget = QWidget()
        
        self.spirograph_widget = SpirographWidget(*get_points(self.spirograph))
        
        R_callback = get_callback(self.spirograph_widget, self.spirograph, self.spirograph.setBigR)
        self.R_slider = VariableSlider("R", self.spirograph.getBigR(), R_callback) 

        r_callback = get_callback(self.spirograph_widget, self.spirograph, self.spirograph.setR)
        self.r_slider = VariableSlider("r", self.spirograph.getR(), r_callback)

        O_callback = get_callback(self.spirograph_widget, self.spirograph, self.spirograph.setO)
        self.O_slider = VariableSlider("O", self.spirograph.getO(), O_callback)

        #assemble the main widget
        main_layout = QVBoxLayout()
        main_layout.addWidget(self.spirograph_widget)
        main_layout.addWidget(self.R_slider)
        main_layout.addWidget(self.r_slider)
        main_layout.addWidget(self.O_slider)

        self.main_widget.setLayout(main_layout)
        self.setCentralWidget(self.main_widget)

class SpirographWidget(QWidget):
    '''widget holding a matplotlib plot showing a spirograph picture'''

    def __init__(self, x, y):
        #initialise the widget
        super().__init__()
        
        #create the matplotlib plot
        fig = Figure()
        self.canvas_widget = FigureCanvasQTAgg(fig)
        
        axes = fig.add_subplot(111)
        self._line, = axes.plot(x, y)  #store the result of the plot so we can edit it later

        #initialising a matplotlib toolbar
        #allows for panning, zooming and saving
        self.toolbar = NavigationToolbar2QT(self.canvas_widget, self)
        
        #create the widget layout
        layout = QVBoxLayout()
        layout.addWidget(self.canvas_widget)
        layout.addWidget(self.toolbar)
        
        self.setLayout(layout)

    def update(self, x, y):
        '''update the plot with new values'''

        self._line.set_data(x, y)
        self._line.figure.canvas.draw()
        
class VariableSlider(QWidget):
    '''widget holding a QSlider that triggers a callback when updated'''

    def __init__(self, var_name, init_value, callback):
        #initialise the widget
        super().__init__()
        
        #creating the internal widgets
        self.name_label = QLabel(var_name + " = " + str(init_value))

        self.slider = QSlider(Qt.Horizontal)
        self.slider.setMinimum(-100)
        self.slider.setMaximum(100)
        self.slider.setValue(int(round(init_value * 10)))  #sliders only accept ints
        self.slider.setTickPosition(QSlider.NoTicks)

        #connect the callback to the slider's valueChanged event
        #the divide by 10 makes the slider behave as if it went from -10 to 10 with a step of 0.1
        #NOTE: could change to using sliderReleased if calculating the points proves too costly
        def on_value_changed(value):
            float_value = value / 10
            self.name_label.setText(var_name + " = " + str(float_value))
            callback(float_value)
        self.slider.valueChanged.connect(on_value_changed)

        #create the widget layout
        layout = QHBoxLayout()
        layout.addWidget(self.name_label)
        layout.addWidget(self.slider)

        self.setLayout(layout)

