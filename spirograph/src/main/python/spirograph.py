import numpy as np

class Spirograph:
    maxT = 100

    def __init__(self, R=6.7, r=-4.0, O=-3.5):
        self.R = R
        self.r = r
        self.O = O

    def calculate_points(self):
        t = np.linspace(0, self.maxT, 500)
        x = (self.R-self.r)*np.cos(t) + self.O*np.cos(((self.R-self.r)/self.r)*t)
        y = (self.R-self.r)*np.sin(t) - self.O*np.sin(((self.R-self.r)/self.r)*t)

        return x, y
    
    #define the same interface as the java spirograph

    def getBigR(self):
        return self.R

    def getR(self):
        return self.r

    def getO(self):
        return self.O

    def setBigR(self, R):
        self.R = R

    def setR(self, r):
        self.r = r

    def setO(self, O):
        self.O = O
