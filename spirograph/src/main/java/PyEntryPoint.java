package william.grant.spirograph;

/** entry point class allowing python access to an instance of Spirograph. */
public class PyEntryPoint {
  private Spirograph spirograph = new Spirograph();

  public Spirograph getSpirograph() {
    return this.spirograph;
  }
}
