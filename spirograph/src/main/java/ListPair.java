package william.grant.spirograph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.math3.linear.RealVector;

/**
 * simple class holding two lists of doubles. allows Spirograph.calculatePoints to return two lists
 */
public class ListPair {
  private List<Double> first;
  private List<Double> second;

  public ListPair(List<Double> first, List<Double> second) {
    this.first = first;
    this.second = second;
  }

  public ListPair(double[] first, double[] second) {
    this.first = Arrays.stream(first).boxed().collect(Collectors.toList());
    this.second = Arrays.stream(second).boxed().collect(Collectors.toList());
  }

  public ListPair(RealVector first, RealVector second) {
    this.first = new ArrayList<Double>();
    this.second = new ArrayList<Double>();

    int last = first.getDimension(); // the vectors are the same size
    for (int n = 0; n < last; n++) {
      this.first.add(first.getEntry(n));
      this.second.add(second.getEntry(n));
    }
  }

  public List<Double> getFirst() {
    return this.first;
  }

  public List<Double> getSecond() {
    return this.second;
  }
}
