package william.grant.spirograph;

import java.io.IOException;
import py4j.GatewayServer;

public class Main {
  /** the main method. starts the gateway server and passes the entrypoint to python. */
  public static void main(String[] args) {
    // start the gateway server
    GatewayServer gatewayServer = new GatewayServer(new PyEntryPoint());
    gatewayServer.start();
    System.out.println("Gateway server Started!");

    // for some reason this make python crash, idk how to fix it
/*
    // start the python front-end
    Process process = null;
    try {
      process = new ProcessBuilder("python3", "src/main/python/main.py").start();
    } catch (IOException e) {
      System.err.println("could not run python!");
      System.exit(1);
    }

    // shut down the gateway server when python terminates
    try {
      process.waitFor(); // block until python completes
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      gatewayServer.shutdown();
    }
*/
  }
}

