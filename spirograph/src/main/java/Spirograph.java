package william.grant.spirograph;

import org.apache.commons.math3.analysis.function.Cos;
import org.apache.commons.math3.analysis.function.Sin;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

/** stores the parameters and calculates the coordinates of a spirograph. */
public class Spirograph {
  private final int maxT = 100;

  private double bigR;
  private double r;
  private double O;

  /** initialise with default values. they make a cool pattern B-). */
  public Spirograph() {
    this.bigR = 6.7;
    this.r = -4.0;
    this.O = -3.5;
  }

  /** initialise with provided values. */
  public Spirograph(double bigR, double r, double O) {
    this.bigR = bigR;
    this.r = r;
    this.O = O;
  }

  /**
   * calculates and returns the points that make up the spirograph pattern. the points make up a
   * parametric curve in the form: x = (R-r)*cos(t) + O*cos(((R-r)/r)*t) y = (R-r)*sin(t) -
   * O*sin(((R-r)/r)*t) where 0 <= t <= maxT
   */
  public ListPair calculatePoints() {
    double bigRMinusR = this.bigR - this.r;
    double bigRMinusROverR = bigRMinusR / this.r;

    // create the linspace
    RealVector t = new ArrayRealVector();
    for (int n = 0; n < 1000; n++) {
      t = t.append((double) n / (1000 - 1) * maxT);
    }

    // calculate the x and y values
    // this looks hideous because java doesn't have operator overloading
    // the equivalent python looks way, way better
    RealVector xValues =
      t.map(new Cos())
        .mapMultiply(bigRMinusR)
        .add(t
          .mapMultiply(bigRMinusROverR)
          .map(new Cos())
          .mapMultiply(this.O));

    RealVector yValues =
      t.map(new Sin())
        .mapMultiply(bigRMinusR)
        .subtract(t
          .mapMultiply(bigRMinusROverR)
          .map(new Sin())
          .mapMultiply(this.O));

    return new ListPair(xValues, yValues);
  }

  public double getBigR() {
    return this.bigR;
  }

  public double getR() {
    return this.r;
  }

  public double getO() {
    return this.O;
  }

  public void setBigR(double bigR) {
    this.bigR = bigR;
  }

  public void setR(double r) {
    this.r = r;
  }

  public void setO(double O) {
    this.O = O;
  }
}

