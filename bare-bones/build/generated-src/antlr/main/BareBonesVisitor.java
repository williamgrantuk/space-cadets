// Generated from BareBones.g4 by ANTLR 4.9.2
package william.grant.BareBones;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link BareBonesParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface BareBonesVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link BareBonesParser#barebones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBarebones(BareBonesParser.BarebonesContext ctx);
	/**
	 * Visit a parse tree produced by {@link BareBonesParser#expressions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressions(BareBonesParser.ExpressionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link BareBonesParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCommand(BareBonesParser.CommandContext ctx);
	/**
	 * Visit a parse tree produced by {@link BareBonesParser#whileblock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileblock(BareBonesParser.WhileblockContext ctx);
	/**
	 * Visit a parse tree produced by {@link BareBonesParser#endline}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEndline(BareBonesParser.EndlineContext ctx);
}