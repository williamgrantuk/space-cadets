// Generated from BareBones.g4 by ANTLR 4.9.2
package william.grant.BareBones;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BareBonesParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, WORD=5, WHITESPACE=6;
	public static final int
		RULE_barebones = 0, RULE_expressions = 1, RULE_command = 2, RULE_whileblock = 3, 
		RULE_endline = 4;
	private static String[] makeRuleNames() {
		return new String[] {
			"barebones", "expressions", "command", "whileblock", "endline"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'while '", "' not 0 do'", "'end'", "';'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, "WORD", "WHITESPACE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "BareBones.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BareBonesParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class BarebonesContext extends ParserRuleContext {
		public ExpressionsContext expressions() {
			return getRuleContext(ExpressionsContext.class,0);
		}
		public TerminalNode EOF() { return getToken(BareBonesParser.EOF, 0); }
		public BarebonesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_barebones; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BareBonesVisitor ) return ((BareBonesVisitor<? extends T>)visitor).visitBarebones(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BarebonesContext barebones() throws RecognitionException {
		BarebonesContext _localctx = new BarebonesContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_barebones);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(10);
			expressions();
			setState(11);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionsContext extends ParserRuleContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public List<WhileblockContext> whileblock() {
			return getRuleContexts(WhileblockContext.class);
		}
		public WhileblockContext whileblock(int i) {
			return getRuleContext(WhileblockContext.class,i);
		}
		public ExpressionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressions; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BareBonesVisitor ) return ((BareBonesVisitor<? extends T>)visitor).visitExpressions(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionsContext expressions() throws RecognitionException {
		ExpressionsContext _localctx = new ExpressionsContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_expressions);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(15); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(15);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case WORD:
					{
					setState(13);
					command();
					}
					break;
				case T__0:
					{
					setState(14);
					whileblock();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(17); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==T__0 || _la==WORD );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandContext extends ParserRuleContext {
		public List<TerminalNode> WORD() { return getTokens(BareBonesParser.WORD); }
		public TerminalNode WORD(int i) {
			return getToken(BareBonesParser.WORD, i);
		}
		public EndlineContext endline() {
			return getRuleContext(EndlineContext.class,0);
		}
		public List<TerminalNode> WHITESPACE() { return getTokens(BareBonesParser.WHITESPACE); }
		public TerminalNode WHITESPACE(int i) {
			return getToken(BareBonesParser.WHITESPACE, i);
		}
		public CommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_command; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BareBonesVisitor ) return ((BareBonesVisitor<? extends T>)visitor).visitCommand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CommandContext command() throws RecognitionException {
		CommandContext _localctx = new CommandContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_command);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(19);
			match(WORD);
			setState(23);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==WHITESPACE) {
				{
				{
				setState(20);
				match(WHITESPACE);
				}
				}
				setState(25);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(26);
			match(WORD);
			setState(27);
			endline();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileblockContext extends ParserRuleContext {
		public TerminalNode WORD() { return getToken(BareBonesParser.WORD, 0); }
		public List<EndlineContext> endline() {
			return getRuleContexts(EndlineContext.class);
		}
		public EndlineContext endline(int i) {
			return getRuleContext(EndlineContext.class,i);
		}
		public ExpressionsContext expressions() {
			return getRuleContext(ExpressionsContext.class,0);
		}
		public WhileblockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileblock; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BareBonesVisitor ) return ((BareBonesVisitor<? extends T>)visitor).visitWhileblock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhileblockContext whileblock() throws RecognitionException {
		WhileblockContext _localctx = new WhileblockContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_whileblock);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(29);
			match(T__0);
			setState(30);
			match(WORD);
			setState(31);
			match(T__1);
			setState(32);
			endline();
			setState(33);
			expressions();
			setState(34);
			match(T__2);
			setState(35);
			endline();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EndlineContext extends ParserRuleContext {
		public List<TerminalNode> WHITESPACE() { return getTokens(BareBonesParser.WHITESPACE); }
		public TerminalNode WHITESPACE(int i) {
			return getToken(BareBonesParser.WHITESPACE, i);
		}
		public EndlineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_endline; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BareBonesVisitor ) return ((BareBonesVisitor<? extends T>)visitor).visitEndline(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EndlineContext endline() throws RecognitionException {
		EndlineContext _localctx = new EndlineContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_endline);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(40);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==WHITESPACE) {
				{
				{
				setState(37);
				match(WHITESPACE);
				}
				}
				setState(42);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(43);
			match(T__3);
			setState(47);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==WHITESPACE) {
				{
				{
				setState(44);
				match(WHITESPACE);
				}
				}
				setState(49);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\b\65\4\2\t\2\4\3"+
		"\t\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\3\2\3\2\3\3\3\3\6\3\22\n\3\r\3\16\3\23"+
		"\3\4\3\4\7\4\30\n\4\f\4\16\4\33\13\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3"+
		"\5\3\5\3\5\3\6\7\6)\n\6\f\6\16\6,\13\6\3\6\3\6\7\6\60\n\6\f\6\16\6\63"+
		"\13\6\3\6\2\2\7\2\4\6\b\n\2\2\2\64\2\f\3\2\2\2\4\21\3\2\2\2\6\25\3\2\2"+
		"\2\b\37\3\2\2\2\n*\3\2\2\2\f\r\5\4\3\2\r\16\7\2\2\3\16\3\3\2\2\2\17\22"+
		"\5\6\4\2\20\22\5\b\5\2\21\17\3\2\2\2\21\20\3\2\2\2\22\23\3\2\2\2\23\21"+
		"\3\2\2\2\23\24\3\2\2\2\24\5\3\2\2\2\25\31\7\7\2\2\26\30\7\b\2\2\27\26"+
		"\3\2\2\2\30\33\3\2\2\2\31\27\3\2\2\2\31\32\3\2\2\2\32\34\3\2\2\2\33\31"+
		"\3\2\2\2\34\35\7\7\2\2\35\36\5\n\6\2\36\7\3\2\2\2\37 \7\3\2\2 !\7\7\2"+
		"\2!\"\7\4\2\2\"#\5\n\6\2#$\5\4\3\2$%\7\5\2\2%&\5\n\6\2&\t\3\2\2\2\')\7"+
		"\b\2\2(\'\3\2\2\2),\3\2\2\2*(\3\2\2\2*+\3\2\2\2+-\3\2\2\2,*\3\2\2\2-\61"+
		"\7\6\2\2.\60\7\b\2\2/.\3\2\2\2\60\63\3\2\2\2\61/\3\2\2\2\61\62\3\2\2\2"+
		"\62\13\3\2\2\2\63\61\3\2\2\2\7\21\23\31*\61";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}