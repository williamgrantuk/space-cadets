grammar BareBones;

/*
 * Parser Rules
 */

barebones   : expressions EOF ;

expressions  : (command | whileblock)+ ;

command     : WORD WHITESPACE* WORD endline ;

whileblock  : 'while ' WORD ' not 0 do' endline expressions 'end' endline ;

endline     : WHITESPACE* ';' WHITESPACE* ;

/*
 * Lexer Rules
 */

WORD            : [a-zA-Z]+ ;

WHITESPACE      : [ \t\n\r]+ ;
