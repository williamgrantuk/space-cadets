package william.grant.BareBones;

public class Register {
  private int value = 0;

  public void clear() {
    this.value = 0;
  }

  public void incr() {
    this.value++;
  }

  public void decr() {
    if (this.value > 0) { // registers cannot hold negative values
      this.value--;
    }
  }

  public void print() {
    System.out.println(this.value);
  }

  public int getValue() {
    return this.value;
  }
}

