package william.grant.BareBones;

import java.io.IOException;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class Main {
  public static void main(String[] args) {

    // get the source file from the path specified in the command line
    CharStream inputStream = null;
    try {
      inputStream = CharStreams.fromFileName(args[0]);
    } catch (IOException e) {
      System.err.println("could not find file " + args[0]);
      System.exit(1);
    }

    // instantiate the generated lexer and parser
    BareBonesLexer lexer = new BareBonesLexer(inputStream);
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    BareBonesParser parser = new BareBonesParser(tokens);

    // instantiate and run the visitor
    BareBonesParser.BarebonesContext context = parser.barebones();
    EvalVisitor visitor = new EvalVisitor();
    visitor.visit(context);
  }
}

