package william.grant.BareBones;

import java.util.HashMap;
import java.util.Map;
import org.antlr.v4.runtime.*;

public class EvalVisitor extends BareBonesBaseVisitor<String> {

  // specify whitch of the two WORDs in the command that we want
  public static final int OPERATOR = 0;
  public static final int OPERAND = 1;

  private Map<String, Register> registerMap = new HashMap<String, Register>();

  @Override
  public String visitBarebones(BareBonesParser.BarebonesContext context) {
    this.visit(context.expressions());

    // print out all the values of the registers
    this.registerMap.forEach(
        (registerName, register) ->
            System.out.println(registerName + String.format(": %d", register.getValue())));

    return null;
  }

  @Override
  public String visitCommand(BareBonesParser.CommandContext context) {

    String registerName = context.WORD(OPERAND).getText();
    Register register = this.getRegister(registerName);

    String operator = context.WORD(OPERATOR).getText();
    switch (operator) {
      case "clear":
        register.clear();
        break;
      case "incr":
        register.incr();
        break;
      case "decr":
        register.decr();
        break;
    }

    return visitChildren(context);
  }

  @Override
  public String visitWhileblock(BareBonesParser.WhileblockContext context) {
    Register register = this.getRegister(context.WORD().getText());

    while (register.getValue() != 0) {
      this.visit(context.expressions());
    }

    return null;
  }

  private Register getRegister(String registerName) {
    Register register = this.registerMap.get(registerName);

    if (register == null) { // the register does not exist
      register = new Register();
      this.registerMap.put(registerName, register);
    }

    return register;
  }
}

