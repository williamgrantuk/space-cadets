# bare bones

an interpreter for the minimalist programing language 'bare bones' <br>
build using "gradle wrapper" then "./gradlew build" <br>
I couldn't figure out how to make a stand-alone jar file using gradle so if you want to run it you must type "./gradlew run --args="name-of-source-file" <br>
sorry if some of the code looks ugly, I had to learn a very complex library in just one week <br>
come to think of it, I think my solution is somewhat over-engineered. <br>
