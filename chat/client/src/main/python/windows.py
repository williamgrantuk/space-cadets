#partially adapted from https://github.com/Permafacture/terminal_windows/blob/master/windows.py

import curses
from curses import textpad

KEY_ENTER = 10

class Window:
    '''manages a curses window. can draw a border and write text'''
    
    def __init__(self, xpos, ypos, width, height, title=None, has_border=False):
        self.xpos = xpos
        self.ypos = ypos
        self.width = width
        self.height = height
        self.title = title
        self.has_border = has_border

        self._window = curses.newwin(height, width, xpos, ypos)

        self.draw_border()
        self.dirty = True

    def write_string(self, x, y, string, effect=0):
        '''write a string to the window with an optional effect'''

        colour = curses.color_pair(curses.COLOR_GREEN) | effect
        self._window.addstr(x, y, string, colour)

    def draw_border(self):
        if self.has_border:
            self._window.border()
        if self.title is not None:
            self.write_string(0, 2, self.title[:self.width-3])

    def update(self):
        self._window.noutrefresh()

    def clear(self):
        for x in range(self.height):
            self._window.addstr(x, 0, ' ' * (width - 2))

class TextEntry:
    '''class managing a window that allows text editing
    runs the callback when enter is pressed, passing the line entered'''

    def __init__(self, xpos, ypos, width, height, callback=None):
        self._window = Window(xpos, ypos, width, height, "Text Entry", True)
        
        if callback is None:
            self.callback = (lambda x: None)
        else:
            self.callback = callback

        self.init_editor()

    def init_editor(self):
        self.input_window = Window(self._window.xpos + 1,
                                   self._window.ypos + 1,
                                   self._window.width - 2,
                                   self._window.height - 2)
        self.input_window._window.keypad(1)
        self.editor = textpad.Textbox(self.input_window._window)
        self.update()

    def update(self):
        self._window.update()
        self.input_window.update()

    def process_key(self, key):
        return_value = None
        
        if key == KEY_ENTER:
            string = self.editor.gather().strip()
            if string != '':
                return_value = self.callback(string)
                self.init_editor()  #'reset' the editor by creating a new one
        elif self.editor.do_command(key):
            self.input_window._window.refresh()  #update the display if a command was entered

        return return_value

class TextDisplay:
    '''class managing a window for displaying lines of text, handles scrolling and line wrapping'''

    def __init__(self, xpos, ypos, width, height):
        self._window = Window(xpos, ypos, width, height, "Chat", True)
        self._lines = list()

    def add_string(self, string, effect=0):
        self._lines.append((string, effect))

    def render_strings(self):
        width = self._window.width - 2
        height = self._window.height - 2
        to_render = self._lines[-height:]  #only display lines that fit on the screen

        frame = list()

        #break list of strings into line wrapped pieces
        for string, effect in to_render:
            old = 0
            while (len(piece := string[old:old+width]) != 0):
                frame.append((piece.ljust(width), effect))
                old += width

        #display the pieces
        for x, part in enumerate(frame[-height:]):
            string, effect = part
            self._window.write_string(x + 1, 1, string, effect)

    def update(self):
        self.render_strings()
        self._window.update()

