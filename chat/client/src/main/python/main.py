import curses
import sys
import argparse

from py4j.java_gateway import JavaGateway

from windows import Window, TextEntry, TextDisplay

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('-u', '--username', default='william')
arg_parser.add_argument('-i', '--ip', default='127.0.0.1')
arg_parser.add_argument('-p', '--port', type=int, default=6666)

def make_callback(username, client, text_display):
    def callback(message):
        text_to_display = client.sendMessage("[" + username + "] " + message)
        text_display.add_string(text_to_display)
        return message

    return callback

if __name__ == '__main__':
    #get the username, IP and port number
    args = arg_parser.parse_args()
    username = args.username
    ip = args.ip
    port = args.port

    #connect to the JVM
    gateway = JavaGateway()
    client = gateway.entry_point.getClient()

    #connect to the chat server
    client.connect(ip, port)

    try:
        #set up the screen
        stdscr = curses.initscr()
        curses.start_color()
        curses.noecho()
        curses.cbreak()
        stdscr.nodelay(1)
        stdscr.keypad(1)

        #create the windows
        maxy, maxx = stdscr.getmaxyx()
        splity = int(maxy * 0.8)

        main_border = Window(0, 0, maxx, maxy, "Chat Program", True)
        text_display = TextDisplay(1, 1, maxx - 1, splity - 1)
        text_entry = TextEntry(splity, 1, maxx - 1, maxy - splity - 1, make_callback(username, client, text_display)) 

        windows = [main_border, text_display, text_entry]

        #main program loop
        while True:
            key = stdscr.getch()
            if key == curses.ERR:  #no key was pressed
                for window in windows:
                    window.update()
                stdscr.refresh()
            else:
                result = text_entry.process_key(key)
                if result == 'quit':
                    break
    finally: 
        #disconnect from the chat server
        client.disconnect()

        #cleanup
        curses.nocbreak()
        stdscr.keypad(0)
        curses.echo()
        curses.endwin()

