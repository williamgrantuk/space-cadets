package william.grant.chat.client;

import py4j.GatewayServer;

public class Main {
    public static void main(String[] args) {
        GatewayServer gateway = new GatewayServer(new PyEntryPoint());
        gateway.start();
        System.out.println("started gateway server!");
    }
}
