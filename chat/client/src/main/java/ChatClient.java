package william.grant.chat.client;

import java.net.*;
import java.io.*;

public class ChatClient {
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    public void connect(String ip, int port) {
        try {
            this.clientSocket = new Socket(ip, port);
        } catch (UnknownHostException e) {
            System.err.printf("could not find a server at %s\n", ip);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("failed to connect to server");
            System.exit(1);
        }

        try {
            this.out = new PrintWriter(this.clientSocket.getOutputStream(), true);
            this.in  = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
        } catch (IOException e) {
            System.err.println("failed to establish connection with server");
            System.exit(1);
        }

        System.out.println("connection established with server!");

    }

    public String sendMessage(String message) {
        this.out.println(message);

        try {
            return this.in.readLine();
        } catch (IOException e) {
            System.err.println("lost connection to server");
            System.exit(1);
            return null;
        }
    }

    public void disconnect() {
        try {
            this.in.close();
            this.out.close();
            this.clientSocket.close();
        } catch (IOException e) {
            System.err.println("failed to properly close connection");
            System.exit(1);
        }
    }
}
