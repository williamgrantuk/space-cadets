package william.grant.chat.server;

import java.net.*;
import java.io.*;

public class ChatServer {
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    public void start(int port) {
        try {
            this.serverSocket = new ServerSocket(port);
            this.clientSocket = this.serverSocket.accept();
            this.out = new PrintWriter(this.clientSocket.getOutputStream(), true);
            this.in = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
        } catch (IOException e) {
            System.err.println("failed to setup server connection");
            System.exit(1);
        }

        System.out.println("connection established!");

        String input;
        try {
            while ((input = this.in.readLine()) != null) {
                System.out.println(input);
                this.out.println(input);

                if (input.equals("quit")) {
                    break;
                }
            }
        } catch (IOException e) {
            System.err.println("lost connection to client");
            System.exit(1);
        }
    }

    public void stop() {
        System.out.println("shutting down server!");

        try {
            this.in.close();
            this.out.close();
            this.clientSocket.close();
            this.serverSocket.close();
        } catch (IOException e) {
            System.err.println("failed to properly stop connection");
            System.exit(1);
        }
    }
}
