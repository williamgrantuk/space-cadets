package william.grant.chat.server;

public class Main {
    private static final int port = 6666;

    public static void main(String[] args) {
        ChatServer server = new ChatServer();
        server.start(port);
        server.stop();
    }
}
