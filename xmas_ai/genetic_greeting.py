import random
import itertools
import sys
from math import inf

PHRASE = 'Jingle bells,\nJingle bells,\nJingle all the way.'
MUT_RATE = 0.05

def random_char():
    #return chr(random.randint(32, 126))
    return random.choice([chr(x) for x in range(32, 127)] + ['\n'])

def mutate(string):
    mutant_string = ''
    for char in string:
        if random.random() < MUT_RATE:
            mutant_string += random_char()
        else:
            mutant_string += char

    if random.random() < MUT_RATE:
        mutant_string += random_char()

    if random.random() < MUT_RATE:
        mutant_string = mutant_string[:-1]

    return mutant_string

def score(string, phrase):
    total_score = 0
    for string_char, phrase_char in itertools.zip_longest(string, phrase):
        if string_char == phrase_char:
            total_score += 1

    total_score -= abs(len(phrase) - len(string))

    return total_score

if __name__ == '__main__':
    #get the phrase from the command line, if one exists
    try:
        phrase = sys.argv[1].strip()
    except IndexError:
        phrase = PHRASE

    #begin with a random, 1 character string
    parent = random_char()
    prev_best = -inf
    while True:
        best_this_gen = score(parent, phrase)
        if best_this_gen > prev_best:
            print(parent, best_this_gen, '\n')
            prev_best = best_this_gen

        if parent == phrase:
            break;
        else:
            next_generation = [mutate(x) for x in itertools.repeat(parent, 100)]
            random.shuffle(next_generation)
            parent = max(next_generation + [parent], key=lambda x: score(x, phrase))

