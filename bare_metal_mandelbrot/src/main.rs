#![no_std]
#![no_main]
#![feature(asm)]

mod vga_buffer;
mod vga_buffer_13h;

use core::panic::PanicInfo;
use bootloader::{entry_point, BootInfo};

const MAX_ITER: u32 = 500;

//provide basic panic handling, simply printing the error message
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    //enter text mode
    unsafe {
        asm!(
            "mov ax, 3",
            "int 10h",
        );
    }
    
    //print the error message
    if let Some(s) = info.payload().downcast_ref::<&str>() {
        vga_buffer::WRITER.lock().write_string(s, 1);
    } else {
        vga_buffer::WRITER.lock().write_string("???", 1);
    }

    loop {}
}

fn mandelbrot(x: f32, y: f32) -> u32 {
    let mut z_real = 0f32;
    let mut z_imag = 0f32;

    for iteration in 0..MAX_ITER {
        if z_real*z_real + z_imag*z_imag > 2.0 {
            return iteration;
        }
        let temp = z_real*z_real - z_imag*z_imag + x;
        z_imag = 2.0 * z_real * z_imag + y;
        z_real = temp;
    }

    return MAX_ITER;
}

entry_point!(text_mode_main);

//the new main function for the mode 1h version
fn mode_13h_main(boot_info: &'static BootInfo) -> ! {

    //enter mode 13h
    unsafe {
        asm!(
            "mov ax, 13h",
            "int 10h",
        );
    }

    //calculate the scale values
    let x_scale = (0.47 + 2.0) / vga_buffer_13h::BUFFER_WIDTH as f32;
    let y_scale = (1.12 + 1.12) / vga_buffer_13h::BUFFER_HEIGHT as f32;

    //calculate the mandelbrot set
    for row in 0..vga_buffer_13h::BUFFER_HEIGHT {
        for col in 0..vga_buffer_13h::BUFFER_WIDTH {
            let scaled_x = -2.0 + col as f32 * x_scale;
            let scaled_y = -1.12 + row as f32 * y_scale;

            //pixel colour should be in range 0x10 to 0x1f (greyscale on the default pallete)
            let pixel = vga_buffer_13h::Pixel(0x10 + ((MAX_ITER - mandelbrot(scaled_x, scaled_y)) * 15 / MAX_ITER) as u8);

            vga_buffer_13h::WRITER.lock().write_pixel(pixel, row, col);
        }
    }

    loop {}
}

//the old main function for the text mode version
fn text_mode_main(boot_info: &'static BootInfo) -> ! {
    //caclulate the scale values
    let x_scale = (0.47 + 2.0) / vga_buffer::BUFFER_WIDTH as f32;
    let y_scale = (1.12 + 1.12) / vga_buffer::BUFFER_HEIGHT as f32;

    //calculate the mandelbrot set
    for row in 1..vga_buffer::BUFFER_HEIGHT {
        for col in 0..vga_buffer::BUFFER_WIDTH {
            let scaled_x = -2.0 + col as f32 * x_scale;
            let scaled_y = -1.12 + row as f32 * y_scale;

            let byte = if mandelbrot(scaled_x, scaled_y) < MAX_ITER {
                b" "
            } else {
                b"*"
            };

            vga_buffer::WRITER.lock().write_byte(byte[0], row, col);
        }
    }

    loop {}
}

