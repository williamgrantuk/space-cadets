use volatile::Volatile;
use lazy_static::lazy_static;
use spin::Mutex;

//don't forget to enter and exit mode 13h!

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
pub struct Pixel(pub u8);

pub const BUFFER_WIDTH: usize = 320;
pub const BUFFER_HEIGHT: usize = 200;

#[repr(transparent)]
struct Buffer {
    pixels: [[Volatile<Pixel>; BUFFER_WIDTH]; BUFFER_HEIGHT],
}

pub struct Writer {
    buffer: &'static mut Buffer,
}

impl Writer {
    pub fn write_pixel(&mut self, pixel: Pixel, row: usize, column: usize) {
        self.buffer.pixels[row][column].write(pixel);
    }
}

lazy_static! {
    pub static ref WRITER: Mutex<Writer> = Mutex::new(Writer {
        buffer: unsafe {&mut *(0x0A000 as *mut Buffer)},
    });
}

